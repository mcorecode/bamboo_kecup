defmodule BambooKecupTest do
  use ExUnit.Case
  alias Bamboo.Email
  import Mimic

  @configuration %{
    endpoint: "https://example.com/send.php",
    server: "server",
    port: 587,
    username: "smtpuser",
    password: "smtppass",
    auth: :always,
    tls: :always,
    ssl: false
  }

  @email [
    from: {"John Doe", "john@example.com"},
    to: [{"Jane Doe", "jane@example.com"}],
    subject: "Hello from Bamboo",
    html_body: "<h1>Bamboo is awesome!</h1>",
    text_body: "*Bamboo is awesome!*"
  ]

  test "requires server in config" do
    assert_raise ArgumentError, ~r/server is required/, fn ->
      BambooKecup.handle_config(configuration(%{server: nil}))
    end
  end

  test "requires port in config" do
    assert_raise ArgumentError, ~r/port is required/, fn ->
      BambooKecup.handle_config(configuration(%{port: nil}))
    end
  end

  test "requires endpoint in config" do
    assert_raise ArgumentError, ~r/endpoint is required/, fn ->
      BambooKecup.handle_config(configuration(%{endpoint: nil}))
    end
  end

  test "sends email" do
    bamboo_email =
      @email
      |> Email.new_email()
      |> Bamboo.Mailer.normalize_addresses()

    bamboo_config = BambooKecup.handle_config(configuration())

    HTTPoison
    |> stub(:post, fn url, data, _headers ->
      assert url == bamboo_config.endpoint

      assert Jason.decode!(data) == %{
               "smtp_configuration" => %{
                 "host" => "server",
                 "port" => 587,
                 "username" => "smtpuser",
                 "password" => "smtppass",
                 "tls" => true,
                 "ssl" => false,
                 "auth" => true
               },
               "from" => %{"name" => "John Doe", "address" => "john@example.com"},
               "to" => %{"name" => "Jane Doe", "address" => "jane@example.com"},
               "subject" => "Hello from Bamboo",
               "text_body" => "*Bamboo is awesome!*",
               "html_body" => "<h1>Bamboo is awesome!</h1>"
             }

      {:ok, %HTTPoison.Response{status_code: 200, body: Jason.encode!(%{"status" => "success"})}}
    end)

    assert BambooKecup.deliver(bamboo_email, bamboo_config) == :ok
  end

  defp configuration(override \\ %{}), do: Map.merge(@configuration, override)
end
