defmodule BambooKecup.MixProject do
  use Mix.Project

  def project do
    [
      app: :bamboo_kecup,
      version: "0.1.0",
      elixir: "~> 1.10",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: []
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:bamboo, "~> 1.6"},
      {:httpoison, "~> 1.0"},
      {:jason, "~> 1.0"},
      {:mimic, "~> 0.1", only: :test}
    ]
  end
end
