defmodule BambooKecup do
  @moduledoc """
  Documentation for `BambooKecup`.
  """

  @behaviour Bamboo.Adapter

  @required_configuration [:endpoint, :server, :port]

  def deliver(
        %{
          from: {from_name, from_address},
          to: [{to_name, to_address}],
          subject: subject,
          text_body: text_body,
          html_body: html_body
        },
        config
      ) do
    payload = %{
      smtp_configuration: to_kecup_smtp_config(config),
      from: %{name: from_name, address: from_address},
      to: %{name: to_name, address: to_address},
      subject: subject,
      text_body: text_body,
      html_body: html_body
    }

    result =
      HTTPoison.post(
        Map.get(config, :endpoint),
        Jason.encode!(payload),
        [{"Accept", "application/json"}, {"Content-Type", "application/json"}]
      )

    case result do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
        case Jason.decode(body) do
          {:ok, %{"status" => "success"}} ->
            :ok

          {:ok, %{"status" => status, "reason" => reason}} ->
            {:error, :send_failed, "#{status}: #{reason}"}

          {:error, %Jason.DecodeError{data: data_status}} ->
            {:error, :invalid_response, data_status}
        end

      {:ok, %HTTPoison.Response{status_code: unexpected_status_code}} ->
        {:error, :invalid_status, unexpected_status_code}

      {:error, %HTTPoison.Error{reason: reason}} ->
        {:error, :http_failure, reason}
    end
  end

  def handle_config(config) do
    config
    |> check_required_configuration()
  end

  def supports_attachments?, do: false

  defp check_required_configuration(config) do
    @required_configuration
    |> Enum.reduce([], &aggregate_errors(config, &1, &2))
    |> raise_on_missing_configuration(config)
  end

  defp aggregate_errors(config, key, errors) do
    config
    |> Map.fetch(key)
    |> build_error(key, errors)
  end

  defp raise_on_missing_configuration([], config), do: config

  defp raise_on_missing_configuration(errors, config) do
    formatted_errors =
      errors
      |> Enum.map(&"* #{&1}")
      |> Enum.join("\n")

    raise ArgumentError, """
    The following settings have not been found in your settings:

    #{formatted_errors}

    They are required to make the SMTP-over-PHP adapter work. Here is your configuration:

    #{inspect(config)}
    """
  end

  defp build_error({:ok, value}, _key, errors) when value != nil, do: errors

  defp build_error(_not_found_value, key, errors) do
    ["Key #{key} is required for SMTP-over-PHP Adapter" | errors]
  end

  defp to_kecup_smtp_config(config) do
    Enum.reduce(config, %{}, &to_kecup_smtp_config/2)
  end

  defp to_kecup_smtp_config({:server, value}, config) when is_binary(value) do
    Map.put(config, :host, value)
  end

  defp to_kecup_smtp_config({:port, value}, config) when is_integer(value) do
    Map.put(config, :port, value)
  end

  defp to_kecup_smtp_config({:username, value}, config) when is_binary(value) do
    Map.put(config, :username, value)
  end

  defp to_kecup_smtp_config({:password, value}, config) when is_binary(value) do
    Map.put(config, :password, value)
  end

  defp to_kecup_smtp_config({:auth, :always}, config) do
    Map.put(config, :auth, true)
  end

  defp to_kecup_smtp_config({:auth, value}, config) when is_boolean(value) do
    Map.put(config, :auth, value)
  end

  defp to_kecup_smtp_config({:tls, :always}, config) do
    Map.put(config, :tls, true)
  end

  defp to_kecup_smtp_config({:tls, value}, config) when is_boolean(value) do
    Map.put(config, :tls, value)
  end

  defp to_kecup_smtp_config({:ssl, value}, config) when is_boolean(value) do
    Map.put(config, :ssl, value)
  end

  defp to_kecup_smtp_config({_key, _value}, config) do
    config
  end
end
